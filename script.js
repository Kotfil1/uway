
function _ () {
    const headerProp = document.querySelectorAll('header');
    const aProp = document.querySelectorAll('a');

    const aPropHTMLCollection = Array.from(aProp)
    const headerHTMLCollection = Array.from(headerProp[0]['children'])



    for (const i in headerHTMLCollection) {
        headerHTMLCollection[i].setAttribute('alt', headerHTMLCollection[i].innerText.toLowerCase())
        headerHTMLCollection[i].setAttribute('tabindex', 1)
        headerHTMLCollection[i].setAttribute('id', 'view-n-' + (+i + 1).toString())
    }

    for (const i in aPropHTMLCollection) {
        aPropHTMLCollection[i].setAttribute('tabindex', 1)
        aPropHTMLCollection[i].setAttribute('class', 'menu-categories__link')
        aPropHTMLCollection[i].setAttribute('alt', `1`)
        aPropHTMLCollection[i].setAttribute('aria-label', `${aPropHTMLCollection[i].innerText.toLowerCase()} category ${(+i + 1)}`)
        aPropHTMLCollection[i].setAttribute('id', 'view-l-' + (+i + 1).toString())
    }

    function doThing(inputs, str) {
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].onkeydown = function (e) {

                if (e.code === str) {
                    console.log(node.tagName)
                    let node = this.nextSibling
                    while (node) {
                        if (node.tagName === 'H2' || node.tagName === 'H1' || node.tagName === 'H3' || node.tagName === 'LI') {
                            node.focus();
                            break;
                        }

                        inputs[0].focus()
                        node = node.nextElementSibling;
                    }

                }
            };
        }
    }

    doThing(headerHTMLCollection, 'KeyH')
    // doThing(aPropHTMLCollection, 'KeyL')

}
_()